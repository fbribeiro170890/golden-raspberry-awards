**Guia de Utilização - Golden Raspberry Awards API**

Requisitos mínimos: JRE 8.
---

## Iniciando a aplicação

1. Abra um novo terminal;
2. Vá para a pasta do projeto, e depois entre na pasta target;
3. Digite java -jar GoldenRaspberryAwards-0.0.1-SNAPSHOT.jar
4. Aguarde a finalização da inicialização;
5. Pronto. A API está iniciada!

---

## Acessando os webservices

1. Obter o(s) vencedor(es), informando um ano; 
*Ex.: http://localhost:8080/golden-raspberry-awards/movie/winners?year=2017 (GET)*

2. Obter os anos que tiveram mais de um vencedor; 
*Ex.: http://localhost:8080/golden-raspberry-awards/movie/winners/years/moreThanOne (GET)*

3. Obter a lista de estúdios, ordenada pelo número de premiações; 
*Ex.: http://localhost:8080/golden-raspberry-awards/studio/winners/winCount (GET)*

4. Obter o produtor com maior intervalo entre dois prêmios, e o que obteve dois prêmios mais rápido; 
*Ex.: http://localhost:8080/golden-raspberry-awards/producer/winners/minMaxInterval (GET)*

5. Excluir um filme. Não deve permitir excluir vencedores. 
*Ex.: http://localhost:8080/golden-raspberry-awards/movie?id=1 (DELETE)*

---

## Rodando os testes
1. Abra um novo terminal;
2. Vá para a pasta do projeto;
3. Digite mvn clean test
4. Acompanhe os resultados!
