package com.texoit;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;
import com.texoit.service.impl.MovieServiceImpl;

/**
 * Testes para o Controller do Movie
 * @author Fabio
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerIntegrationTest {
    
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private MovieServiceImpl movieService;
    
    /**
     * verifica se enviando um ano que contém filmes, 
     * o resultado está correto
     * @throws Exception
     */
    @Test
    public void givenYear_whenGetWinnerMovies_thenReturnJsonArray()
      throws Exception {
         
        Movie movie1;
        Movie movie2;
        Integer year; 
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2011);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2011);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2));
        
        year = 2011;
        
        MovieRepo.getInstance().setMovies(movies);

        when(movieService.getWinnersByYear(year)).thenCallRealMethod();
        
        mvc.perform(get("/movie/winners/")
        	.param("year", String.valueOf(year))
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(2)))
			.andExpect(jsonPath("$[0].title", is(movie1.getTitle())))
			.andExpect(jsonPath("$[1].title", is(movie2.getTitle())));

    }
    
    /**
     * verifica se, enviando um ano em que não hajam filmes,
     * nenhum filme é exibido
     * @throws Exception
     */
    @Test
    public void givenYear_whenDontGetWinnerMovies_thenReturnEmptyJson()
      throws Exception {
         
    	Movie movie1;
        Movie movie2;
        Integer year; 
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2015);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2015);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2));
        
        year = 2008;

        MovieRepo.getInstance().setMovies(movies);
        
        when(movieService.getWinnersByYear(year)).thenCallRealMethod();
        
        mvc.perform(get("/movie/winners/")
        	.param("year", String.valueOf(year))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(0)));
    }
    
    /**
     * verificando ano com múltiplos filmes vencedores
     * @throws Exceptions
     */
    @Test
    public void givenNothing_whenGetMultipleWinners_thenReturnJsonArray()
      throws Exception {
        
        Movie movie1;
        Movie movie2;
        Movie movie3;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2015);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2015);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
    			.setProducers(Arrays.asList("pr 2"))
    			.setStudios(Arrays.asList("std 3"))
    			.setWinner(true).setYear(2011);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3));

        MovieRepo.getInstance().setMovies(movies);
        
        when(movieService.getYearsWithMoreThanOneWinner()).thenCallRealMethod();
        
        mvc.perform(get("/movie/winners/years/moreThanOne")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$.years.[0].winnerCount", is(2)));
    }
    
    /**
     * verificando quando não há mais de um vencedor no mesmo ano
     * @throws Exception
     */
    @Test
    public void givenNothing_whenDontGetMultipleWinners_thenReturnEmptyJson()
      throws Exception {
        
        Movie movie1;
        Movie movie2;
        Movie movie3;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2013);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2015);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
    			.setProducers(Arrays.asList("pr 2"))
    			.setStudios(Arrays.asList("std 3"))
    			.setWinner(true).setYear(2011);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3));

        MovieRepo.getInstance().setMovies(movies);
        
        when(movieService.getYearsWithMoreThanOneWinner()).thenCallRealMethod();
        
        mvc.perform(get("/movie/winners/years/moreThanOne")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$.years", hasSize(0)));
    }
    
    /**
     * deletando um filme
     * @throws Exception
     */
    @Test
    public void givenMovieId_whenDeleteMovie_thenReturnNoContentStatus()
      throws Exception {
        
        Movie movie1;
        Movie movie2;
        Movie movie3;
        List<Movie> movies;
        Long idToDelete;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2013);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2015);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
    			.setProducers(Arrays.asList("pr 2"))
    			.setStudios(Arrays.asList("std 3"))
    			.setWinner(false).setYear(2011);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3));

        idToDelete = 3l;
        
        MovieRepo.getInstance().setMovies(movies);
        
        when(movieService.deleteById(idToDelete)).thenCallRealMethod();
        
        mvc.perform(delete("/movie")
        	.param("id", String.valueOf(idToDelete))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNoContent());
    }
    
    /**
     * tentando deletar um filme vencedor, não deve permitir
     * @throws Exception
     */
    @Test
    public void givenWinnerMovieId_whenDontDeleteMovie_thenReturnConflictStatus()
      throws Exception {
        
        Movie movie1;
        Movie movie2;
        Movie movie3;
        List<Movie> movies;
        Long idToDelete;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2013);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2015);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
    			.setProducers(Arrays.asList("pr 2"))
    			.setStudios(Arrays.asList("std 3"))
    			.setWinner(false).setYear(2011);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3));

        idToDelete = 1l;
        
        MovieRepo.getInstance().setMovies(movies);
        
        when(movieService.deleteById(idToDelete)).thenCallRealMethod();
        
        mvc.perform(delete("/movie")
        	.param("id", String.valueOf(idToDelete))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isConflict());
    }
}
