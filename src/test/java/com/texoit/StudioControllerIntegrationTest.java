package com.texoit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;
import com.texoit.service.impl.StudioServiceImpl;

/**
 * Testes para o Controller do Movie
 * @author Fabio
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudioControllerIntegrationTest {
	

    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private StudioServiceImpl studioService;
    
    /**
     * Agrupar os estúdios vitoriosos corretamente
     * @throws Exception
     */
    @Test
    public void givenNothing_whenGetWinnersByCount_thenReturnJsonArray()
      throws Exception {
         
        Movie movie1;
        Movie movie2;
        Movie movie3;
        Movie movie4;
        Movie movie5;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2011);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 2", "std 4"))
			.setWinner(true).setYear(2010);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
			.setProducers(Arrays.asList("pr 1", "pr 3"))
			.setStudios(Arrays.asList("std 2", "std 3"))
			.setWinner(true).setYear(2013);
        
        movie4 = new Movie().setId(4l).setTitle("movie 4")
			.setProducers(Arrays.asList("pr 1", "pr 4"))
			.setStudios(Arrays.asList("std 1", "std 3"))
			.setWinner(true).setYear(2014);
        
        movie5 = new Movie().setId(5l).setTitle("movie 5")
    			.setProducers(Arrays.asList("pr 4"))
    			.setStudios(Arrays.asList("std 1", "std 3"))
    			.setWinner(false).setYear(2014);
        
        movies = new ArrayList<>();
        movies.addAll(
        	Arrays.asList(movie1, movie2, movie3, movie4, movie5));
        
        MovieRepo.getInstance().setMovies(movies);

        when(studioService.getStudioWinnersByOrderByWinCount()).thenCallRealMethod();
        
        mvc.perform(get("/studio/winners/winCount")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk())
			.andExpect(jsonPath("$.studios.[0].winCount", is(3)))
			.andExpect(jsonPath("$.studios.[1].winCount", is(2)));
    }
 

    /**
     * Testar quando nenhum estúdio ganhou
     * @throws Exception
     */
    @Test
    public void givenNothing_whenGetNoWinners_thenReturnJsonArray()
      throws Exception {
         
        Movie movie1;
        Movie movie2;
        Movie movie3;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(false).setYear(2011);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(false).setYear(2010);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
			.setProducers(Arrays.asList("pr 1", "pr 3"))
			.setStudios(Arrays.asList("std 5", "std 6"))
			.setWinner(false).setYear(2013);
        
        movies = new ArrayList<>();
        movies.addAll(
        	Arrays.asList(movie1, movie2, movie3));
        
        MovieRepo.getInstance().setMovies(movies);

        when(studioService.getStudioWinnersByOrderByWinCount()).thenCallRealMethod();
        
        mvc.perform(get("/studio/winners/winCount")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk())
			.andExpect(jsonPath("$.studios", hasSize(0)));
    }
}
