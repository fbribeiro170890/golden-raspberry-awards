package com.texoit;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;
import com.texoit.service.impl.ProducerServiceImpl;

/**
 * Testes para o Controller do Producer
 * @author Fabio
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProducerControllerIntegrationTest {
	   
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private ProducerServiceImpl producerService;
    
    /**
     * verifica intervalo entre vitórias dos produtores(min e max)
     * @throws Exception
     */
    @Test
    public void givenNothing_whenGetProducerWinnerInterval_thenReturnJsonArray()
      throws Exception {
         
        Movie movie1;
        Movie movie2;
        Movie movie3;
        Movie movie4;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(1995);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(1998);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(true).setYear(2001);
     
        movie4 = new Movie().setId(4l).setTitle("movie 4")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(true).setYear(2002);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3, movie4));
        
        MovieRepo.getInstance().setMovies(movies);

        when(producerService.getProducerWinnersByMinMaxInterval()).thenCallRealMethod();
        
        mvc.perform(get("/producer/winners/minMaxInterval")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk())
			.andExpect(jsonPath("$.min.[0].interval", is(4)))
			.andExpect(jsonPath("$.max.[0].interval", is(6)));
    }

    /**
     * verificando quando não há vencedores
     * @throws Exception
     */
    @Test
    public void givenNothing_whenGetProducerNoWinner_thenReturnJsonArray()
      throws Exception {
         
        Movie movie1;
        Movie movie2;
        Movie movie3;
        Movie movie4;
        List<Movie> movies;
        
        movie1 = new Movie().setId(1l).setTitle("movie 1")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(false).setYear(1995);
     
        movie2 = new Movie().setId(2l).setTitle("movie 2")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(false).setYear(1998);
        
        movie3 = new Movie().setId(3l).setTitle("movie 3")
			.setProducers(Arrays.asList("pr 1", "pr 2"))
			.setStudios(Arrays.asList("std 1", "std 2"))
			.setWinner(false).setYear(2001);
     
        movie4 = new Movie().setId(4l).setTitle("movie 4")
			.setProducers(Arrays.asList("pr 3"))
			.setStudios(Arrays.asList("std 3", "std 4"))
			.setWinner(false).setYear(2002);
        
        movies = new ArrayList<>();
        movies.addAll(Arrays.asList(movie1, movie2, movie3, movie4));
        
        MovieRepo.getInstance().setMovies(movies);

        when(producerService.getProducerWinnersByMinMaxInterval()).thenCallRealMethod();
        
        mvc.perform(get("/producer/winners/minMaxInterval")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
        	.andExpect(status().isOk())
			.andExpect(jsonPath("$.min", hasSize(0)))
			.andExpect(jsonPath("$.max", hasSize(0)));
    }
}
