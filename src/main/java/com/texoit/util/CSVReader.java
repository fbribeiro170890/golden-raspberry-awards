package com.texoit.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;

/**
 * Classe para realizar a leitura de um CSV interno no projeto
 * @author Fabio
 * 
 */
public class CSVReader {
	
	private MovieRepo movieRepo;
	
	{ movieRepo = MovieRepo.getInstance(); }
	
	private static final String inputFilePath = "csv/movielist.csv";
	
	/**
	 * realiza a leitura do CSV
	 * @return List<Movie>
	 */
	public List<Movie> processInputFile() {
	    List<Movie> inputList = new ArrayList<>();
	    try {
	    	Resource resource = new ClassPathResource(inputFilePath);
	    	BufferedReader br = 
    			new BufferedReader(new InputStreamReader(resource.getInputStream()));
			inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
			br.close();
	    } catch (IOException e) {
	    	System.err.println(e);
	    }
	    
	    return inputList;
	}
	
	/**
	 * funcão para mapear uma linha do CSV em entidades
	 */
	private Function<String, Movie> mapToItem = (line) -> {
		  String[] p = line.split(";");
		  Movie movie = new Movie();
		  movie.setYear(Integer.parseInt(p[0]));
		  movie.setTitle(p[1]);
		  
		  List<String> studios = Arrays.asList(p[2].split(", | and "));
		  movie.setStudios(studios);
		  
		  List<String> producers = Arrays.asList(p[3].split(", | and "));
		  movie.setProducers(producers);
		  
		  movie.setWinner(p.length == 5 && p[4].equals("yes"));
		  movieRepo.addMovies(movie);
		  
		  return movie;
	};

}
