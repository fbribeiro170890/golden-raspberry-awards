package com.texoit.repo;

import java.util.ArrayList;
import java.util.List;

import com.texoit.entity.Movie;

/**
 * Repositório dos Movies
 * @author Fabio
 *
 */
public class MovieRepo {

	private List<Movie> movies;
	private static MovieRepo instance;

	private MovieRepo() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Singleton para o repositório
	 * @return MovieRepo
	 */
	public static MovieRepo getInstance(){
		if (instance == null) instance = new MovieRepo();
		return instance;
	}

	@Override
	public String toString() {
		return "MovieRepo [movies=" + movies + "]";
	}

	/**
	 * Adicionar um novo movie à lista
	 * @param movie
	 */
	public void addMovies(Movie movie) {
		if (movies == null)
			movies = new ArrayList<>();
		if (!movies.contains(movie)){
			movie.setId((long) (movies.size()+1));
			movies.add(movie);
		}
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

}
