package com.texoit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.dto.ProducerIntervalMinMaxDTO;
import com.texoit.service.ProducerService;
import com.texoit.service.impl.ProducerServiceImpl;

/**
 * Controller para produtores
 * @author Fabio
 *
 */
@RestController
@RequestMapping(value="/producer")
public class ProducerController implements ProducerService{
	
	@Autowired
	ProducerServiceImpl producerService;
	
	/**
	 * Obter o produtor com maior intervalo entre dois prêmios, 
	 * e o que obteve dois prêmios mais rápido
	 * @return ResponseEntity<ProducerIntervalMinMaxDTO>
	 */
	@RequestMapping(value="/winners/minMaxInterval",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProducerIntervalMinMaxDTO> getProducerWinnersByMinMaxInterval(){
		return producerService.getProducerWinnersByMinMaxInterval();
	}

}
