package com.texoit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.dto.StudioWinnerListDTO;
import com.texoit.service.StudioService;
import com.texoit.service.impl.StudioServiceImpl;

/**
 * controller para os estúdios
 * @author Fabio
 *	
 */
@RestController
@RequestMapping(value="/studio")
public class StudioController implements StudioService {
	
	@Autowired
	StudioServiceImpl studioService;
	
	/**
	 * Obter a lista de estúdios, ordenada pelo número de premiações
	 * @return ResponseEntity<StudioWinnerListDTO>
	 */
	@RequestMapping(value="/winners/winCount",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudioWinnerListDTO> getStudioWinnersByOrderByWinCount(){
		return studioService.getStudioWinnersByOrderByWinCount();
	}

}
