package com.texoit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.dto.MovieWinnerListDTO;
import com.texoit.entity.Movie;
import com.texoit.service.MovieService;
import com.texoit.service.impl.MovieServiceImpl;

/**
 * Controller para os filmes
 * @author Fabio
 *
 */
@RestController
@RequestMapping(value="/movie")
public class MovieController implements MovieService{

	@Autowired
	MovieServiceImpl movieService;
	
	/**
	 * Obter o(s) vencedor(es), informando um ano
	 * @param year
	 * @return ResponseEntity<List<Movie>>
	 */
	@RequestMapping(value="/winners",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Movie>> getWinnersByYear(@RequestParam Integer year){
		return movieService.getWinnersByYear(year);
	}
	
	/**
	 * Obter os anos que tiveram mais de um vencedor
	 * @return ResponseEntity<MovieWinnerListDTO>
	 */
	@RequestMapping(value="/winners/years/moreThanOne",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MovieWinnerListDTO> getYearsWithMoreThanOneWinner(){
		return movieService.getYearsWithMoreThanOneWinner();
	}
	
	/**
	 * Excluir um filme. Não deve permitir excluir vencedores. 
	 * @param id
	 * @return ResponseEntity<?>
	 */
	@RequestMapping(value="",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteById(@RequestParam Long id){
		return movieService.deleteById(id);
	}
	
}
