package com.texoit.dto;

import java.util.List;

/**
 * Lista de estúdios vencedores
 * 
 * @author Fabio
 *
 */
public class StudioWinnerListDTO {

	private List<StudioWinnerCountDTO> studios;

	public StudioWinnerListDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "StudioWinnerListDTO [studios=" + studios + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */

	public List<StudioWinnerCountDTO> getStudios() {
		return studios;
	}

	public StudioWinnerListDTO setStudios(List<StudioWinnerCountDTO> studios) {
		this.studios = studios;
		return this;
	}

}
