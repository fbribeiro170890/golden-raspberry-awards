package com.texoit.dto;

import java.util.List;

/**
 * Lista de filmes vencedores
 * @author Fabio
 *
 */
public class MovieWinnerListDTO {

	private List<MovieWinnerByYearDTO> years;

	public MovieWinnerListDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MovieWinnerListDTO [years=" + years + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public List<MovieWinnerByYearDTO> getYears() {
		return years;
	}

	public MovieWinnerListDTO setYears(List<MovieWinnerByYearDTO> years) {
		this.years = years;
		return this;
	}

}
