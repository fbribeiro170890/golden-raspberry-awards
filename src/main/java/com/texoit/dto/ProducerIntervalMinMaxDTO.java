package com.texoit.dto;

import java.util.List;

/**
 * Intervalo Mínimo e máximo para produtores
 * @author Fabio
 *
 */
public class ProducerIntervalMinMaxDTO {

	private List<ProducerIntervalDTO> min;
	private List<ProducerIntervalDTO> max;

	public ProducerIntervalMinMaxDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ProducerIntervalMinMaxDTO [min=" + min + ", max=" + max + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public List<ProducerIntervalDTO> getMin() {
		return min;
	}

	public ProducerIntervalMinMaxDTO setMin(List<ProducerIntervalDTO> min) {
		this.min = min;
		return this;
	}

	public List<ProducerIntervalDTO> getMax() {
		return max;
	}

	public ProducerIntervalMinMaxDTO setMax(List<ProducerIntervalDTO> max) {
		this.max = max;
		return this;
	}

}
