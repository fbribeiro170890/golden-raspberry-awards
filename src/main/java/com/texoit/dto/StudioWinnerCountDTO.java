package com.texoit.dto;

/**
 * contagem de estudios vencedores
 * @author Fabio
 *
 */
public class StudioWinnerCountDTO {

	private String name;
	private Long winCount;

	public StudioWinnerCountDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "StudioWinnerCountDTO [name=" + name + ", winCount=" + winCount + "]";
	}
	
	/*
	 * Gets and sets com padrão builder
	 */
	
	public String getName() {
		return name;
	}

	public StudioWinnerCountDTO setName(String name) {
		this.name = name;
		return this;
	}

	public Long getWinCount() {
		return winCount;
	}

	public StudioWinnerCountDTO setWinCount(Long winCount) {
		this.winCount = winCount;
		return this;
	}

}
