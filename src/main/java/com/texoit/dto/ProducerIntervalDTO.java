package com.texoit.dto;

/**
 * Intervalo entre prêmios de produtores
 * @author Fabio
 *
 */
public class ProducerIntervalDTO {

	private String producer;
	private Integer interval;
	private Integer previousWin;
	private Integer followingWin;

	public ProducerIntervalDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ProducerIntervalDTO [producer=" + producer + ", interval=" + interval + ", previousWin=" + previousWin
				+ ", followingWin=" + followingWin + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public String getProducer() {
		return producer;
	}

	public ProducerIntervalDTO setProducer(String producer) {
		this.producer = producer;
		return this;
	}

	public Integer getInterval() {
		return interval;
	}

	public ProducerIntervalDTO setInterval(Integer interval) {
		this.interval = interval;
		return this;
	}

	public Integer getPreviousWin() {
		return previousWin;
	}

	public ProducerIntervalDTO setPreviousWin(Integer previousWin) {
		this.previousWin = previousWin;
		return this;
	}

	public Integer getFollowingWin() {
		return followingWin;
	}

	public ProducerIntervalDTO setFollowingWin(Integer followingWin) {
		this.followingWin = followingWin;
		return this;
	}

}
