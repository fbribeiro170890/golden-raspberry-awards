package com.texoit.dto;

/**
 * DTO para filmes vencedores por ano
 * @author Fabio
 *
 */
public class MovieWinnerByYearDTO {

	private Integer year;
	private Long winnerCount;

	public MovieWinnerByYearDTO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MovieWinnerByYearDTO [year=" + year + ", winnerCount=" + winnerCount + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public Integer getYear() {
		return year;
	}

	public MovieWinnerByYearDTO setYear(Integer year) {
		this.year = year;
		return this;
	}

	public Long getWinnerCount() {
		return winnerCount;
	}

	public MovieWinnerByYearDTO setWinnerCount(Long winnerCount) {
		this.winnerCount = winnerCount;
		return this;
	}

}
