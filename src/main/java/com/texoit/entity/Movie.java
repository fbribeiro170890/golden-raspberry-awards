package com.texoit.entity;

import java.util.List;

/**
 * Entity para Movie
 * @author Fabio
 *
 */
public class Movie {

	private Long id;
	private Integer year;
	private String title;
	private List<String> studios;
	private List<String> producers;
	private Boolean winner;

	public Movie() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", year=" + year + ", title=" + title + ", studios=" + studios + ", producers="
				+ producers + ", winner=" + winner + "]";
	}

	/*
	 * Gets and sets com padrão builder
	 */
	
	public Long getId() {
		return id;
	}

	public Movie setId(Long id) {
		this.id = id;
		return this;
	}

	public Integer getYear() {
		return year;
	}

	public Movie setYear(Integer year) {
		this.year = year;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public Movie setTitle(String title) {
		this.title = title;
		return this;
	}

	public List<String> getStudios() {
		return studios;
	}

	public Movie setStudios(List<String> studios) {
		this.studios = studios;
		return this;
	}

	public List<String> getProducers() {
		return producers;
	}

	public Movie setProducers(List<String> producers) {
		this.producers = producers;
		return this;
	}

	public Boolean getWinner() {
		return winner;
	}

	public Movie setWinner(Boolean winner) {
		this.winner = winner;
		return this;
	}

}
