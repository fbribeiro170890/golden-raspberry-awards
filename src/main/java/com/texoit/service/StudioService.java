package com.texoit.service;

import org.springframework.http.ResponseEntity;

import com.texoit.dto.StudioWinnerListDTO;

/**
 * Interface de Studios
 * @author Fabio
 *
 */
public interface StudioService {

	/**
	 * Obter a lista de estúdios, ordenada pelo número de premiações
	 * @return ResponseEntity<StudioWinnerListDTO>
	 */
	public ResponseEntity<StudioWinnerListDTO> getStudioWinnersByOrderByWinCount();
}
