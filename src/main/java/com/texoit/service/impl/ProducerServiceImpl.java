package com.texoit.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.texoit.dto.ProducerIntervalDTO;
import com.texoit.dto.ProducerIntervalMinMaxDTO;
import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;
import com.texoit.service.ProducerService;

/**
 * Service para produtores
 * @author Fabio
 *
 */
@Service
public class ProducerServiceImpl implements ProducerService{

	/**
	 * Obter o produtor com maior intervalo entre dois prêmios, 
	 * e o que obteve dois prêmios mais rápido
	 * @return ResponseEntity<ProducerIntervalMinMaxDTO>
	 */
	public ResponseEntity<ProducerIntervalMinMaxDTO> getProducerWinnersByMinMaxInterval(){

		List<Movie> winnerMovies;
		List<ProducerIntervalDTO> producersInterval;
		List<ProducerIntervalDTO> producersIntervalSorted;
		List<ProducerIntervalDTO> minProducerIntervalValues;
		List<ProducerIntervalDTO> maxProducerIntervalValues;
		ProducerIntervalMinMaxDTO intervalMinMax;
		
		producersInterval = new ArrayList<>();
		minProducerIntervalValues = new ArrayList<>();
		maxProducerIntervalValues = new ArrayList<>();
		intervalMinMax = new ProducerIntervalMinMaxDTO();		
		
		winnerMovies = MovieRepo.getInstance().getMovies().stream()
			.filter(m -> m.getWinner())
			.sorted((f1, f2) -> Long.compare(f1.getYear(), f2.getYear()))
			.collect(Collectors.toList());
		
		winnerMovies.stream()
			.map(Movie::getProducers)
			.collect(Collectors.toList())
			.stream()
	        .flatMap(List::stream)
			.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
			.entrySet()
			.stream()
			.filter(p -> p.getValue() > 1)
			.collect(Collectors.toList())
			.forEach(producer -> {
				winnerMovies.forEach(movie -> {
					if (movie.getProducers().contains(producer.getKey())){
						ProducerIntervalDTO producerInterval;
						producerInterval = new ProducerIntervalDTO();
						Integer index;
						index = 0;
						
						for(ProducerIntervalDTO pi : producersInterval){
							if (pi.getProducer().equals(producer.getKey())) {
								producerInterval = pi;							
								index = producersInterval.indexOf(pi);
							}							
						}
						
						if (producerInterval.getProducer() == null) {
							producerInterval.setProducer(producer.getKey());
							producerInterval.setPreviousWin(movie.getYear());
							producersInterval.add(producerInterval);
						} else {
							producerInterval.setFollowingWin(movie.getYear());
							producerInterval.setInterval(
								movie.getYear() - producerInterval.getPreviousWin());
							producersInterval.set(index, producerInterval);
						}
						
					}
				});
			});
		
		producersIntervalSorted = 
			producersInterval.stream()
			.sorted((f1, f2) -> Long.compare(f1.getInterval(), f2.getInterval()))
			.collect(Collectors.toList());
		
		minProducerIntervalValues = 
			producersIntervalSorted.stream()
			.filter(pis -> 
				pis.getInterval().equals(producersIntervalSorted.get(0).getInterval()))
			.collect(Collectors.toList());
		
		maxProducerIntervalValues = 
				producersIntervalSorted.stream()
				.filter(pis -> 
					pis.getInterval().equals(producersIntervalSorted.get(
						producersIntervalSorted.size()-1).getInterval()))
				.collect(Collectors.toList());
		
		intervalMinMax.setMin(minProducerIntervalValues);
		intervalMinMax.setMax(maxProducerIntervalValues);
		
		return new ResponseEntity<>(intervalMinMax, HttpStatus.OK);
	}
	
}
