package com.texoit.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.texoit.dto.MovieWinnerByYearDTO;
import com.texoit.dto.MovieWinnerListDTO;
import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;
import com.texoit.service.MovieService;

/**
 * Service para Movies
 * @author Fabio
 *
 */
@Service
public class MovieServiceImpl implements MovieService{
	
	/**
	 * Obter o(s) vencedor(es), informando um ano
	 * @param year
	 * @return ResponseEntity<List<Movie>>
	 */
	public ResponseEntity<List<Movie>> getWinnersByYear(Integer year){
		
		List<Movie> winners;
		
		winners = MovieRepo.getInstance().getMovies().stream()
			.filter(m -> m.getYear().equals(year) && m.getWinner())
			.collect(Collectors.toList());
		
		return new ResponseEntity<>(winners, HttpStatus.OK);
	}
	
	/**
	 * Obter os anos que tiveram mais de um vencedor
	 * @return ResponseEntity<MovieWinnerListDTO>
	 */
	public ResponseEntity<MovieWinnerListDTO> getYearsWithMoreThanOneWinner(){
		
		MovieWinnerListDTO yearWithMoreThanOneWinner;
		List<MovieWinnerByYearDTO> winners;

		yearWithMoreThanOneWinner = new MovieWinnerListDTO();
		winners = new ArrayList<>();

		MovieRepo.getInstance().getMovies().stream()
			.filter(m -> m.getWinner())
			.map(Movie::getYear)
			.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
			.forEach((year, count) -> { 
				if (count > 1){
					MovieWinnerByYearDTO winner;
					winner = new MovieWinnerByYearDTO();
					winner.setYear(year).setWinnerCount(count);
					winners.add(winner);
				} 
			});
		
		yearWithMoreThanOneWinner.setYears(winners);
		
		return new ResponseEntity<>(yearWithMoreThanOneWinner, HttpStatus.OK);
	}
	
	/**
	 * Excluir um filme. Não deve permitir excluir vencedores.
	 * 
	 * @param id
	 * @return ResponseEntity<?>
	 */
	public ResponseEntity<?> deleteById(Long id){
		
		Optional<Movie> movieToDelete;
		
		movieToDelete = MovieRepo.getInstance().getMovies().stream()
			.filter(m -> m.getId().equals(id))
			.findFirst();
		
		if (!movieToDelete.equals(Optional.empty()) && !movieToDelete.get().getWinner()){	
			MovieRepo.getInstance().getMovies().remove(movieToDelete.get());
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(null, HttpStatus.CONFLICT);
		}
		
	}

}
