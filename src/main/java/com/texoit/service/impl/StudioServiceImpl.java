package com.texoit.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.texoit.dto.StudioWinnerCountDTO;
import com.texoit.dto.StudioWinnerListDTO;
import com.texoit.entity.Movie;
import com.texoit.repo.MovieRepo;

/**
 * service para os estúdios
 * @author Fabio
 *	
 */
@Service
public class StudioServiceImpl {
	
	/**
	 * Obter a lista de estúdios, ordenada pelo número de premiações
	 * @return ResponseEntity<StudioWinnerListDTO>
	 */
	public ResponseEntity<StudioWinnerListDTO> getStudioWinnersByOrderByWinCount(){
		
		StudioWinnerListDTO winnersSorted;
		List<StudioWinnerCountDTO> winners;

		winnersSorted = new StudioWinnerListDTO();
		winners = new ArrayList<>();

		MovieRepo.getInstance().getMovies().stream()
			.filter(m -> m.getWinner())
			.map(Movie::getStudios)
			.collect(Collectors.toList())
			.stream()
	        .flatMap(List::stream)
	        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
	        .entrySet()
	        .stream()
	        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
	        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, 
    			(e1, e2) -> e2, LinkedHashMap::new))
	        .forEach((name, count) -> {
				StudioWinnerCountDTO winner;
				winner = new StudioWinnerCountDTO();
				winner.setName(name).setWinCount(count);
				winners.add(winner);
			});
		
		winnersSorted.setStudios(winners);
		
		return new ResponseEntity<>(winnersSorted, HttpStatus.OK);
	}
}
