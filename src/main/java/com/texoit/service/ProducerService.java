package com.texoit.service;

import org.springframework.http.ResponseEntity;

import com.texoit.dto.ProducerIntervalMinMaxDTO;

/**
 * Interface de Producer
 * @author Fabio
 *
 */
public interface ProducerService {

	/**
	 * Obter o produtor com maior intervalo entre dois prêmios, 
	 * e o que obteve dois prêmios mais rápido
	 * @return ResponseEntity<ProducerIntervalMinMaxDTO>
	 */
	public ResponseEntity<ProducerIntervalMinMaxDTO> getProducerWinnersByMinMaxInterval();
}
