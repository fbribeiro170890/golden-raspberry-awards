package com.texoit.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.texoit.dto.MovieWinnerListDTO;
import com.texoit.entity.Movie;

/**
 * Interface de Movie
 * @author Fabio
 *
 */
public interface MovieService {

	/**
	 * Obter o(s) vencedor(es), informando um ano
	 * @param year
	 * @return ResponseEntity<List<Movie>>
	 */
	public ResponseEntity<List<Movie>> getWinnersByYear(Integer year);

	/**
	 * Obter os anos que tiveram mais de um vencedor
	 * @return ResponseEntity<MovieWinnerListDTO>
	 */
	public ResponseEntity<MovieWinnerListDTO> getYearsWithMoreThanOneWinner();

	/**
	 * Excluir um filme. Não deve permitir excluir vencedores. 
	 * @param id
	 * @return ResponseEntity<?>
	 */
	public ResponseEntity<?> deleteById(Long id);

}
